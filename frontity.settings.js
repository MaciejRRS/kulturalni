const settings = {
  "name": "kulturalni",
  "state": {
    "frontity": {
      "url": "https://kulturalni-frontity.kulturalni.civ.pl",
      "title": "Test Frontity Blog",
      "description": "WordPress installation for Frontity development"
    }
  },
  "packages": [
    {
      "name": "kulturalni"
    },
    {
      "name": "@frontity/wp-source",
      "state": {
        "source": {
          "url": "https://kulturalni-frontity.kulturalni.civ.pl",
          params: {
            per_page: 9,
          },
          taxonomies: [
            {
              taxonomy: "category_extended", // taxonomy slug
              endpoint: "category_extended", // REST API endpoint
              postTypeEndpoint: "posts", // endpoint from which posts from this taxonomy are fetched
            }
          ]
        },
        
      }
    },
    "@frontity/tiny-router",
    "@frontity/html2react"
  ]
};

export default settings;
