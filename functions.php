<?php

register_nav_menus(array(
	'primary' => __('Primary Menu', 'kulturalni'),
));




add_filter('wpcf7_autop_or_not', '__return_false');

// Register thumbnails
add_theme_support('post-thumbnails');
add_image_size('homepage-thumb', 385, 302);

//add option page to panel (ACF)
// if( function_exists('acf_add_options_page') ) {
// 	acf_add_options_page();

// }

//add option page to panel (ACF)
// if( function_exists('acf_add_options_page') ) {
// 	acf_add_options_page('Blog widget');
// }









// Register Taxonomy category_extended
function register_taxonomy_category_extended()
{
	$labels = [
		'name' => _x('Categories', 'taxonomy general name'),
		'singular_name' => _x('Category', 'taxonomy singular name'),
		'search_items' => __('Search Categories'),
		'all_items' => __('All Categories'),
		'parent_item' => __('Parent Category'),
		'parent_item_colon' => __('Parent Category:'),
		'edit_item' => __('Edit Category'),
		'update_item' => __('Update Category'),
		'add_new_item' => __('Add New Category'),
		'new_item_name' => __('New Category Name'),
		'menu_name' => __('Category extended'),
	];
	$args = [
		'hierarchical' => true, // make it hierarchical (like categories)
		'labels' => $labels,
		'show_ui' => true,
		'show_in_rest' => true,
		'show_admin_column' => true,
		'query_var' => true,
		'rewrite' => ['slug' => 'category_extended'],
	];
	register_taxonomy('category_extended', ['post'], $args);
}
add_action('init', 'register_taxonomy_category_extended');

















// // ebooks
// // Dodawanie nowego typu postów START
// function create_ebooks_posttype(){
// 	$args = array(
// 	'label' => 'ebook',
// 	'labels' => array(
// 	'name' => __('ebook'),
// 	'singular_name' => __('ebook'),
// 	'add_new' => __('Dodaj ebook'),
// 	'add_new_item' => __('Dodaj Nowy ebook'),
// 	'edit_item' => __('Edytuj ebook'),
// 	'new_item' => __('Nowy ebook'),
// 	'view_item' => __('Zobacz ebook'),
// 	'search_items' => __('Szukaj ebook'),
// 	'not_found' => __('Nie Znaleziono ebooka'),
// 	'not_found_in_trash' => __('Brak ebooka w koszu'),
// 	'all_items' => __('Wszystkie ebooki'),
// 	'archives' => __('Zarchiwizowane ebooki'),
// 	'insert_into_item' => __('Dodaj do ebooka'),
// 	'uploaded_to_this_item' => __('Sciągnięto do bieżącego ebooka'),
// 	'featured_image' => __('Zdjęcie ebooka'),
// 	'set_featured_image' => __('Ustaw Zdjęcie ebooka'),
// 	'remove_featured_image' => __('Usuń Zdjęcie ebooka'),
// 	'use_featured_image' => __('Użyj Zdjęcie ebooka'),
// 	'menu_name' => __('ebook')
// 	),
// 	'description' => __('Typ Postu zawiera treść dla Pradnika'),
// 	'public' => true,
// 	'exclude_from_search' => false,
// 	'publicly_queryable' => true,
// 	'show_ui' => true,
// 	'show_in_nav_menus' => true,
// 	'show_in_menu' => true,
// 	'show_in_admin_bar' => true,
// 	'menu_position' => 5,
// 	'menu_icon' => 'dashicons-coffee',
// 	'supports' => array('title','editor','author','thumbnail','excerpt','revisions','page-attributes'),
// 	'has_archive' => false,
// 	'hierarchical' => false,
// 	'show_in_rest' => true,
// 	'rewrite' => array('slug'=>'ebooks','with_front'=>false),
// 	'capabilities' => array(
// 	'edit_post' => 'update_core',
// 	'read_post' => 'update_core',
// 	'delete_post' => 'update_core',
// 	'edit_posts' => 'update_core',
// 	'edit_others_posts' => 'update_core',
// 	'delete_posts' => 'update_core',
// 	'publish_posts' => 'update_core',
// 	'read_private_posts' => 'update_core'
// 	),
// 	);
// 	register_post_type('ebooks',$args);
// 	}
// 	add_action('init','create_ebooks_posttype',0);

// 	function register_taxonomy_category_ebooks() {
// 	$labels = [
// 	'name' => _x('Categories', 'taxonomy general name'),
// 	'singular_name' => _x('Category', 'taxonomy singular name'),
// 	'search_items' => __('Search Categories'),
// 	'all_items' => __('All Categories'),
// 	'parent_item' => __('Parent Category'),
// 	'parent_item_colon' => __('Parent Category:'),
// 	'edit_item' => __('Edit Category'),
// 	'update_item' => __('Update Category'),
// 	'add_new_item' => __('Add New Category'),
// 	'new_item_name' => __('New Category Name'),
// 	'menu_name' => __('Kategorie'),
// 	];
// 	$args = [
// 	'hierarchical' => true, // make it hierarchical (like categories)
// 	'labels' => $labels,
// 	'show_ui' => true,
// 	'show_in_rest' => true,
// 	'show_admin_column' => true,
// 	'query_var' => true,
// 	'rewrite' => ['slug' => 'category_ebooks'],
// 	];
// 	register_taxonomy('category_ebooks', ['ebooks'], $args);
// 	}
// 	add_action('init', 'register_taxonomy_category_ebooks');	


// 	function my_plugin_rest_route_for_post( $route, $post ) {
// 		if ( $post->post_type === 'ebooks' ) {
// 			$route = '/wp/v2/ebooks/' . $post->ID;
// 		}

// 		return $route;
// 	}
// 	add_filter( 'rest_route_for_post', 'my_plugin_rest_route_for_post', 10, 2 );






add_filter('rest_post_collection_params', function ($params, WP_Post_Type $post_type) {
	if ('post' === $post_type->name && isset($params['per_page'])) {
		$params['per_page']['maximum'] = 1000;
	}

	return $params;
}, 10, 2);






function wl_page_by_slug($slug)
{

	echo "<pre>";
	print_r($slug['slug']);
	echo "</pre>";

	//remember if here should always return in json
	return array($slug['slug']);
}

add_action('rest_api_init', function () {
	register_rest_route('wl/v1', 'post/(?P<slug>[a-zA-Z0-9-]+)', [
		'methods' => 'GET',
		'callback' => 'wl_page_by_slug',
	]);
});

//** *Enable upload for webp image files.*/
function webp_upload_mimes($existing_mimes)
{
	$existing_mimes['webp'] = 'image/webp';
	return $existing_mimes;
}
add_filter('mime_types', 'webp_upload_mimes');


function get_filtred_posts($data)
{
	if($data['place'] != 'false' && $data['date'] != 'false' && $data['name'] != 'false'){
		$posts = get_posts(array(
			'numberposts' => -1,
			'category_extended' =>  $data['category_name'],
			'meta_query'	=> array(                          
				'relation'		=> 'AND',
				array(
					'key'	  	=> 'date_month',
					'value'	  	=>  $data['date'], // 202101
					'compare'	=> 'LIKE'
				),
				array(
					'key'	  	=> 'place_filter',
					'value'	  	=>  $data['place'],
					'compare'	=> 'LIKE'
				),
				array(
					'key'	  	=> 'slug_filter',
					'value'	  	=>  $data['name'],
					'compare'	=> 'LIKE'
				),
			),
		));
	} elseif ($data['place'] != 'false' && $data['date'] != 'false') {
		$posts = get_posts(array(
			'numberposts' => -1,
			'category_extended' =>  $data['category_name'],
			'meta_query'	=> array(                          
				'relation'		=> 'AND',
				array(
					'key'	  	=> 'date_month',
					'value'	  	=>  $data['date'], // 202101
					'compare'	=> 'LIKE'
				),
				array(
					'key'	  	=> 'place_filter',
					'value'	  	=>  $data['place'],
					'compare'	=> 'LIKE'
				),
			),
		));

	} elseif ($data['place'] != 'false' && $data['name'] != 'false') {
		$posts = get_posts(array(
			'numberposts' => -1,
			'category_extended' =>  $data['category_name'],
			'meta_query'	=> array(                          
				'relation'		=> 'AND',
				array(
					'key'	  	=> 'place_filter',
					'value'	  	=>  $data['place'],
					'compare'	=> 'LIKE'
				),
				array(
					'key'	  	=> 'slug_filter',
					'value'	  	=>  $data['name'],
					'compare'	=> 'LIKE'
				),
			),
		));

	} elseif ($data['date'] != 'false' && $data['name'] != 'false') {
		$posts = get_posts(array(
			'numberposts' => -1,
			'category_extended' =>  $data['category_name'],
			'meta_query'	=> array(                          
				'relation'		=> 'AND',
				array(
					'key'	  	=> 'date_month',
					'value'	  	=>  $data['date'], // 202101
					'compare'	=> 'LIKE'
				),
				
				array(
					'key'	  	=> 'slug_filter',
					'value'	  	=>  $data['name'],
					'compare'	=> 'LIKE'
				),
			),
		));

	} else if ($data['place'] != 'false') {
		$posts = get_posts(array(
			'numberposts' => -1,
			'category_extended' =>  $data['category_name'],
			'meta_query'	=> array(                          
				'relation'		=> 'AND',
				array(
					'key'	  	=> 'place_filter',
					'value'	  	=>  $data['place'],
					'compare'	=> 'LIKE'
				),
			),
		));

	} else if ($data['date'] != 'false') {
		$posts = get_posts(array(
			'numberposts' => -1,
			'category_extended' =>  $data['category_name'],
			'meta_query'	=> array(                          
				'relation'		=> 'AND',
				array(
					'key'	  	=> 'date_month',
					'value'	  	=>  $data['date'], // 202101
					'compare'	=> 'LIKE'
				),
			),
		));
	} else if ($data['name'] != 'false') {
		$posts = get_posts(array(
			'numberposts' => -1,
			'category_extended' =>  $data['category_name'],
			array(
				'key'	  	=> 'slug_filter',
				'value'	  	=>  $data['name'],
				'compare'	=> 'LIKE'
			),
		));
	} else {
		return 'error';
	}
	

	if (empty($posts)) {
		return 'error';
	};

	return $posts;
}

add_action('rest_api_init', function () {
	register_rest_route('category/v1', '/category_extended/(?P<category_name>.+)/(?P<name>.+)/(?P<date>.+)/(?P<place>.+)', array(
		'methods' => 'GET',
		'callback' => 'get_filtred_posts'
	));
});


if (function_exists('acf_add_options_page')) {

	acf_add_options_page(array(
		'page_title' 	=> 'Adds',
		'menu_title'	=> 'Adds',
		'menu_slug' 	=> 'adds',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
}
