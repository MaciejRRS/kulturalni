export const CheckForPass = (adds) => {
    let passedAdds = []
    let today = new Date();
    const dd = String(today.getDate()).padStart(2, '0');
    const mm = String(today.getMonth() + 1).padStart(2, '0');
    const yyyy = today.getFullYear();
    today = dd + '/' + mm + '/' + yyyy;
    const endDate = (el) => {
        if (el.end_date.substr(-4, 4) > today.substr(-4, 4)) {
            return true
        } else if (el.end_date.substr(-4, 4) == today.substr(-4, 4)) {
            if (el.end_date.substr(-7, 2) > today.substr(-7, 2)) {
                return true
            } else if (el.end_date.substr(-7, 2) == today.substr(-7, 2)) {
                if (el.end_date.substr(0, 2) > today.substr(0, 2)) {
                    return true
                } else {
                    return false
                }
            } else {
                return false
            }
        } else {
            return false
        }
    }
    const startDate = (el) => {
        if (el.start_date.substr(-4, 4) < today.substr(-4, 4)) {
            return true
        } else if (el.start_date.substr(-4, 4) == today.substr(-4, 4)) {
            if (el.start_date.substr(-7, 2) < today.substr(-7, 2)) {
                return true
            } else if (el.start_date.substr(-7, 2) == today.substr(-7, 2)) {
                if (el.start_date.substr(0, 2) <= today.substr(0, 2)) {
                    return true
                } else {
                    return false
                }
            } else {
                return false
            }
        } else {
            return false
        }
    }

    adds.forEach(el => {
        if (endDate(el) && startDate(el) && el.number_of_views) {
            passedAdds.push(el)
        }
    });
    return passedAdds
}

export const ChooseAdds = (passedAdds, numberOfAdds) => {
    let choosenAdds = []
    for (let i; choosenAdds.length < numberOfAdds; i) {
        let pos = Math.floor(Math.random() * choosenAdds.length)
        let choosen = passedAdds[pos]

        // axios number_of_views -= 1 

        choosenAdds.push(choosen)
    }
    return choosenAdds
}