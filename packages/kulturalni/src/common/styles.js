import { styled } from "frontity"

export const Container = styled.div`
    max-width: 1640px;
    width: calc(100% - 160px);
    margin: 0 auto;
    height: 100%;
    @media(max-width: 1198px){
        width: calc(100% - 80px);
    }
    @media(max-width: 764px){
        width: calc(100% - 60px);
    }
`