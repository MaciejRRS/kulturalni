import React, { useState, useEffect } from 'react'
import { connect } from "frontity"
import { styled } from "frontity"
import { Container } from './../../common/styles'
import Item from './components/Item'
import Link from "@frontity/components/link"
import Filter from './components/Filter'
import axios from 'axios'

const Wrapper = styled.main`
    margin: 0 0 30px;
`

const Title = styled.h2`
    font-size: 40px;
    font-weight: bold;
    padding-left: 50px;
    color: ${props => props.color};
    text-transform: uppercase;
    @media(max-width: 1198px){
        font-size: 25px;
        padding-left: 16px;
    }
`

const Icon = styled.div`
    background-color: ${props => props.color};
    width: 92px;
    height: 92px;
    display: flex;
    justify-content: center;
    align-items: center;
    img{
        max-width: 50%;
        max-height: 50%;
        width: 100%;
    }
    @media(max-width: 1198px){
        width: 62px;
        height: 62px;
    }
`

const Flex = styled.div`
    display: flex;
    align-items: center;
`

const Text = styled.div`
    margin: 30px 0;
    color: #707070;
    font-size: 15px;
    line-height: 20px;
`

const Grid = styled.div`
    display: grid;
    grid-template-columns: 1fr 1fr 1fr 1fr ;
    grid-column-gap: 50px;
    grid-row-gap: 50px;
    @media(max-width: 1600px){
        grid-column-gap: 16px;
        grid-row-gap: 16px;
        grid-template-columns: 1fr 1fr 1fr ;
    }
    @media(max-width: 964px){
        grid-template-columns: 1fr 1fr;
    }
    @media(max-width: 600px){
        grid-template-columns: 1fr;
    }
`

const Navigation = styled(Link)`
    background-color: ${props => props.color};
    color: #ffffff;
    margin: 50px auto 0;
    display: block;
    width: 225px;
    height: 45px;
    line-height: 45px;
    text-align: center;
    text-decoration: none;
    font-size: 20px;
`


const CategoryFilterPage = ({ state }) => {

    let data = state.source.get(state.router.link)
    let currentCategory = state.source.category_extended[data.id]

    const [filtredArray, changeFiltredArray] = useState([])
    const [idArray, changeIdArray] = useState('')
    const [nameFilter, changeNameFilter] = useState('')
    const [dateFilter, changeDateFilter] = useState('')
    const [placeFilter, changePlaceFilter] = useState('')
    const [error, changeError] = useState('')

    useEffect(() => {
        if (idArray) {
            axios.get(`https://kulturalni-frontity.kulturalni.civ.pl/wp-json/wp/v2/posts?include=${idArray}`)
                .then((res) => { changeFiltredArray(res.data)})
        } else {
            changeFiltredArray([])
        }
    }, [idArray])

    return (
        <Wrapper>
            <Filter changeIdArray={changeIdArray} changeError={changeError} changePlaceFilter={changePlaceFilter} changeDateFilter={changeDateFilter} changeNameFilter={changeNameFilter} state={state} currentCategory={currentCategory} />
            <Container>
                <div>
                    {filtredArray.length
                        ? <span>
                            Nazwa: {nameFilter ? nameFilter : `Nie wybrana`}
                            Data: {dateFilter ? dateFilter : `Nie wybrana`}
                            Miejsce: {placeFilter ? placeFilter : `Nie wybrana`}
                        </span>
                        : null
                    }
                    {error}
                </div>
                <Flex>
                    <Icon color={currentCategory.acf.color}><img src={currentCategory.acf.icon} alt="ikona kategorii" /></Icon>
                    <Title color={currentCategory.acf.color}>{currentCategory.name}</Title>
                </Flex>
                <Text>{currentCategory.description}</Text>
                <Grid>
                    {filtredArray.length
                        ? <>
                            {filtredArray.map((el, index) =>
                                <Item key={index} el={el} state={state} currentCategory={currentCategory} />
                            )}
                        </>
                        : <>
                            {data.items.map((el, index) =>
                                <Item key={index} el={el} state={state} currentCategory={currentCategory} />
                            )}
                        </>
                    }

                </Grid>
                <Flex>
                    {data.previous && <Navigation link={data.previous} color={currentCategory.acf.color}>Pokaż nowsze posty</Navigation>}
                    {data.next && <Navigation link={data.next} color={currentCategory.acf.color}>Pokaż starsze posty</Navigation>}
                </Flex>
            </Container>
        </Wrapper>
    )
}

export default connect(CategoryFilterPage)