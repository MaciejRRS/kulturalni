import React, { useState } from 'react'
import BackGround from '../../../common/sprites/filter_bg.jpg'
import { styled } from "frontity"
import Loupe from './../../../common/sprites/loupe.svg'
import TimeTable from './../../../common/sprites/timetable.svg'
import PlaceHolder from './../../../common/sprites/placeholder.svg'
import Link from "@frontity/components/link"
import axios from 'axios'

const Wrapper = styled.section`
    background-image: url(${props => props.BackGround});
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    min-height: 600px;
    margin-bottom: 50px;
    position: relative;
    @media(max-width: 1196px){
        margin-bottom: 450px;
    }
`

const Form = styled.form`
    min-height: 250px;
    background-color: #333333DD;
    position: absolute;
    bottom: 0;
    left: 50%;
    max-width: 1640px;
    width: calc(100% - 160px);
    transform: translateX(-50%);
    @media(max-width: 1196px){
        min-height: 562px;
        transform: translateX(-50%) translateY(400px);
    }
    @media(max-width: 764px){
        width: calc(100% - 40px);

    }
`

const FormContent = styled.div`
    width: 100%;
    max-width: calc(100% - 400px);
    min-width: 960px;
    height: 150px;
    margin: 50px auto;
    display: flex;
    justify-content: space-between;
    flex-direction: column;
    @media(max-width: 1196px){
        min-width: unset;
        max-width: 100%;
    }
`

const Flex = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: flex-end;

    @media(max-width: 1196px){
        flex-direction: column;
        align-items: center;
    }
`

const InputWrapper = styled.div`
    display: flex;
    flex-direction: column;
    width: 25%;
    position: relative;
    @media(max-width: 1196px){
        width: 450px;
    }
    @media(max-width: 764px){
        width: calc(100% - 32px);
    }
`

const Input = styled.input`
    height: 50px;
    line-height: 50px;
    width: 100%;
    border: 1px solid #333333;
    margin-top: 15px;
    box-sizing: border-box;
    padding: 0 25px 0 50px;
`

const Icon = styled.img`
    position: absolute;
    left: 6px;
    bottom: 6px;

    &.time{
        left: 10px;
    }

    &.place{
        left: 14px;
        bottom: 10px;
    }
`

const Label = styled.label`
    font-size: 20px;
    text-transform: uppercase;
    font-weight: bold;
    color: #ffffff;
    @media(max-width: 1196px){
        margin-top: 32px;
    }
`

const Legend = styled.legend`
    color: #ffffff;
    font-size: 26px;
    text-transform: uppercase;
    font-weight: bold;
    @media(max-width: 1196px){
        max-width: 450px;
        width: calc(100% - 32px);
        margin: 0 auto;
    }
`

const NavLink = styled.button`
    height: 50px;
    width: 50px;
    background-color: #ffffff;
    display: flex;
    justify-content: center;
    align-items: center;
    border: 1px solid #333333;
    box-sizing: border-box;
    text-decoration: none;
    span{
        display: none;
    }
    @media(max-width: 1196px){
        margin-top: 40px;
        width: auto;
        padding: 5px 30px;

        span{
            display: block;
            padding-left: 8px;
            color: #707070;
            text-transform: uppercase;
            font-size: 20px;
        }
    }
    
`

const Filter = (props) => {

    const [name, changeName] = useState('')
    const [date, changeDate] = useState('')
    const [place, changePlace] = useState('')


    const acf_data = props.state.source.get('/acf/')
    const acf = props.state.source[acf_data.type][acf_data.id]['acf']['category_filter_page']


    const Submit = (event) => {
        event.preventDefault();
        let dateL = date ? date.substr(0, 4) + date.substr(-2, 2) : false
        let nameL = name ? name : false
        let placeL = place ? place : false

        props.changeNameFilter(name)
        props.changeDateFilter(date)
        props.changePlaceFilter(place)

        axios.get(`https://kulturalni-frontity.kulturalni.civ.pl/wp-json/category/v1/category_extended/${props.currentCategory.slug}/${nameL}/${dateL}/${placeL}`)
            .then((res) => {
                debugger
                if (res.data === 'error') {
                    props.changeError('not found')
                    props.changeIdArray('')
                } else {
                    props.changeError('')
                    let str = ''
                    res.data.forEach(el => {
                        str += '' + el.ID + ','
                    })
                    props.changeIdArray(str)
                }
            })
    }

    return (
        <Wrapper BackGround={BackGround}>
            <Form onSubmit={(event) => { Submit(event) }}>
                <FormContent>
                    <Legend>{acf.form_title}</Legend>
                    <Flex>
                        <InputWrapper>
                            <Label htmlFor='name'>{acf.name_label}</Label>
                            <Input placeholder={acf.name_placeholder} id='name' value={name} onChange={event => changeName(event.target.value)} />
                            <Icon src={Loupe} />
                        </InputWrapper>
                        <InputWrapper>
                            <Label htmlFor='date'>{acf.date_label}</Label>
                            <Input placeholder={acf.date_placeholder} type="month" id='date' value={date} onChange={event => changeDate(event.target.value)} />
                            <Icon src={TimeTable} className='time' />
                        </InputWrapper>
                        <InputWrapper>
                            <Label htmlFor='place'>{acf.place_label}</Label>
                            <Input placeholder={acf.place_placeholder} id='place' value={place} onChange={event => changePlace(event.target.value)} />
                            <Icon src={PlaceHolder} className='place' />
                        </InputWrapper>
                        <div>
                            <NavLink><img src={Loupe} /><span>{acf.submit_text}</span></NavLink>
                        </div>
                    </Flex>
                </FormContent>
            </Form>
        </Wrapper>
    )
}

export default Filter