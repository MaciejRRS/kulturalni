import React from 'react'
import { styled } from "frontity"
import Link from "@frontity/components/link"

const Item = styled.div`
    position: relative;
    width: 100%;
    min-height: 248px;
    background-image: url(${props => props.background});
    background-position: center;
    background-size: cover;
    background-repeat: no-repeat;

    &:nth-child(1){
        grid-column-start: 1;
        grid-column-end: 3;
        grid-row-start: 1;
        grid-row-end: 3;
        min-height: 546px;

        h3{
            font-size: 20px;
        }

        @media(max-width: 1600px){
            min-height: 512px;
        }

        @media(max-width: 964px){
            grid-column-start: unset;
            grid-column-end: unset;
            grid-row-start: unset;
            grid-row-end: unset;
            min-height: 248px;
        }
    }
`

const ItemTitle = styled.h3`
    position: absolute;
    bottom: 15px;
    left: 15px;
    font-size: 15px;
    font-weight: bold;
    color: #ffffff;
`

const Icon = styled.div`
    position: absolute;
    left: 0;
    top: 0;
    background-color: ${props => props.color};
    width: 92px;
    height: 92px;
    display: flex;
    justify-content: center;
    align-items: center;
    border-bottom-right-radius: 15px;
    img{
        max-width: 50%;
        max-height: 50%;
        width: 100%;
    }
    @media(max-width: 1198px){
        width: 62px;
        height: 62px;
    }
`

const NavLink = styled(Link)`
    position: absolute;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
`

const LayoutItem = (props) => {
    debugger
    const data = props.state.source[props.el.type][props.el.id]
    return (
        <Item alt={props.alt} background={data.acf.img}>
            <NavLink link={data.link} >
                <Icon color={props.currentCategory.acf.color}><img src={props.currentCategory.acf.icon} alt="ikona kategorii" /></Icon>
                <ItemTitle>{data.title.rendered}</ItemTitle>
            </NavLink>
        </Item>
    )
}

export default LayoutItem