import React from 'react';
import { styled } from "frontity"

const Spin = styled.section`
    height: calc(100vh - 376px);
`

const Error = () => {
    return(
        <Spin>
            404
        </Spin>
    )
}

export default Error