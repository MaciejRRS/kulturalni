import React from 'react'
import { styled } from "frontity"
import Link from "@frontity/components/link"
import { Container } from './../../common/styles'
import Send from './../../common/sprites/send-button.svg'
import mail from './../../common/sprites/email.svg'
import phone from './../../common/sprites/call.svg'
import facebook from './../../common/sprites/facebook.svg'
import instagram from './../../common/sprites/instagram.svg'
import { connect } from "frontity"


const Footer = styled.footer`
`

const MainPart = styled.section`
    background-color: #1E2B33;
    padding: 50px 0;
`

const Copyright = styled.section`
    height: 100px;
`

const Flex = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    height: 100%;
    
    @media(max-width: 1196px){
        flex-direction: column;
    }
`

const CopyrightFlex = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    height: 100%;
    span{
    }
`

const Form = styled.div`
    max-width: 400px;
    width: 100%;
    @media(max-width: 1196px){
        max-width: 600px;
        width: 100%;
    }   
`

const Legend = styled.legend`
    font-size: 27px;
    font-weight: bold;
    color: #ffffff;
`

const FormAnnotation = styled.p`
    color: #ffffff;
    font-size: 17px;
    font-weight: normal;
`

const InputWrapper = styled.div`
    position: relative;
`

const Input = styled.input`
    width: 100%;
    line-height: 50px;
    font-size: 14px;
    border: none;
    box-sizing: border-box;
    padding: 0 15px;    
    margin: 30px 0;
    &::placeholder{
        line-height: 50px;
        font-size: 14px;
    }
`

const Submit = styled.button`
    position: absolute;
    background-color: transparent;
    border: none;
    right: 0;
    top: 50%;
    padding: 12px;
    transform: translateY(-50%);
`

const Kontakt = styled.div`
    text-align: center;
    display: grid;
    grid-row-gap: 20px;
    @media(max-width: 1196px){
        margin: 35px 0 70px;
        width: 100%;
        max-width: 600px;
        text-align: left;
    }  
`

const KontaktGrid = styled.div`
    display: grid;
    grid-row-gap: 20px;
    @media(max-width: 1196px){
        grid-template-columns: 1fr 1fr 1fr;
    }   
    @media(max-width: 764px){
        grid-template-columns: 1fr;
        text-align: center;
    }
`

const KontaktTitle = styled.h4`
    text-transform: uppercase;
    color: #ffffff;
    font-size: 17px;
    font-weight: bold;
    @media(max-width: 1196px){
        font-size: 27px;
    }
    @media(max-width: 764px){
        text-align: center;
    }
`

const KontaktLink = styled.a`
    display: block;
    padding-left: 35px;
    color: #ffffff;
    font-size: 17px;
    position: relative;
    text-decoration: none;

    &::before{
        content: url(${props => props.icon});
        left: 0;
        position: absolute;
    }

    @media(max-width: 764px){
        padding-left: 0px;
        &::before{
            position: relative;
            padding-right: 10px;
        }
    }
`

const Icon = styled.img`
    margin: 0 10px;
`

const IconFlex = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
`

const Links = styled.div`
    display: grid;
    max-width: 400px;
    width: 100%;
    grid-template-columns: 1fr 1fr 1fr;
    grid-column-gap: 50px;
    grid-row-gap: 15px;
    a{
        color: #ffffff;
        text-decoration: none;
        font-size: 17px;
        text-align: right;
    }
    @media(max-width: 1196px){
        max-width: 600px;
        width: 100%;

        a{
            text-align: left;
        }
    } 
    @media(max-width: 764px){
        grid-template-columns: 1fr 1fr;

        a{
            text-align: center;
        }
    }
`

const FooterComponent = ({ state }) => {

    const acf_data = state.source.get('/acf/')
    const acf = state.source[acf_data.type][acf_data.id]['acf']['footer']

    return (
        <Footer>
            <MainPart>
                <Container>
                    <Flex>
                        <Form>
                            <Legend>{acf.form_titlte}</Legend>
                            <InputWrapper>
                                <Input placeholder={acf.form_placeholder} />
                                <Submit>
                                    <img src={Send} alt="submit button icon" />
                                </Submit>
                            </InputWrapper>
                            <FormAnnotation>
                                {acf.form_text}
                            </FormAnnotation>
                        </Form>
                        <Kontakt>
                            <KontaktTitle>{acf.kontakt_title}</KontaktTitle>
                            <KontaktGrid>
                                <KontaktLink href={`mailto:${acf.kontakt_email}`} icon={mail}>{acf.kontakt_email}</KontaktLink>
                                <KontaktLink href={`tel:${acf.kontakt_phone}`} icon={phone}>{acf.kontakt_phone}</KontaktLink>
                                <IconFlex>
                                    <a href={acf.kontakt_facebook}><Icon src={facebook} alt='facebook' /></a>
                                    <a href={acf.kontakt_instagram}><Icon src={instagram} alt='instagram' /></a>
                                </IconFlex>
                            </KontaktGrid>
                        </Kontakt>
                        <Links>
                            {acf.links.map(el => <Link link={el.link}>{el.name}</Link>)}
                        </Links>
                    </Flex>
                </Container>
            </MainPart>
            <Copyright>
                <Container>
                    <CopyrightFlex>
                        <div>
                            <b>Projekt i realizacja:</b> {acf.copyright_left}
                        </div>
                        <div>
                            <span>{acf.copyright_right}</span>
                        </div>
                    </CopyrightFlex>
                </Container>
            </Copyright>
        </Footer>
    )
}

export default connect(FooterComponent)