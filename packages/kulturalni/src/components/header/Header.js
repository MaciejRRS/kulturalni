import React, { useState } from 'react'
import { styled } from "frontity"
import Link from "@frontity/components/link"
import { Container } from './../../common/styles'
import Loupe from './../../common/sprites/loupe.svg'
import { connect } from "frontity"


const Header = styled.header`
    height: 84px;
    box-shadow: 0 3px 6px 0 #00000016;
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    z-index: 99;
    background: #fff;
`

const Flex = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    height: 100%;
`

const Logo = styled.div`
    line-height: 65px;
    font-size: 49px;
`

const Menu = styled.button`
    width: 55px;
    height: 55px;
    position: relative;
    background-color: #95CC5E;
    display: flex;
    justify-content: center;
    align-items: center;
    border: none;
    cursor: pointer;

        &::after{
            content: "";
            position: absolute;
            width: 35px;
            height: 5px;
            background-color: #fff;
            display: block;
            transform: ${props => props.isOpened ? 'translateX(-50px) translateY(-11px)' : 'translateY(-11px)'};
            transition: .2s linear;
        }

        &::before{
            content: "";
            position: absolute;
            width: 35px;
            height: 5px;
            background-color: #fff;
            display: block;
            transform: ${props => props.isOpened ? 'translateX(-50px) translateY(11px)' : 'translateY(11px)'};
            transition: .2s linear;
        }

    span{
        width: 35px;
        height: 5px;
        background-color: #fff;
        display: block;
        transform: ${props => props.isOpened ? 'translateX(50px)' : 'translateX(0px)'};
        transition: .2s linear;
        pointer-events: ${props => props.isOpened ? 'none' : 'all'};
    }
`

const NavLink = styled(Link)`
    text-decoration: none;
    color: #707070;
    transition: .2s linear;

    &:hover{
        color: #000000;
    }

`

const Navigation = styled.nav`
    width: 400px;
    position: fixed;
    right: 0;
    top: 0;
    bottom: 0;  
    background-color: #000000CC;
    transform: ${props => props.isOpened ? 'translateX(0px)' : 'translateX(400px)'};
    transition: .2s linear;
    z-index: 100;
    overflow-y: scroll;

    @media(max-width: 764px){
        width: 100%;
        transform: ${props => props.isOpened ? 'translateX(0px)' : 'translateX(100%)'};
    }
`

const Close = styled.button`
    position: absolute;
    right: 10px;
    top: 10px;
    width: 55px;
    height: 55px;
    background-color: #000000;
    border: none;
    display: flex;
    justify-content: center;
    align-items: center;
    cursor: pointer;
    &::after{
        content: "";
        position: absolute;
        width: 35px;
        height: 2px;
        background-color: #fff;
        display: block;
        transform: rotateZ(-45deg);
        transition: .2s linear;
    }

    &::before{
        content: "";
        position: absolute;
        width: 35px;
        height: 2px;
        background-color: #fff;
        display: block;
        transform: rotateZ(45deg);
        transition: .2s linear;
    }
`

const Search = styled.div`
    padding-top: 90px;
    height: 70px;

    input{
        width: 100%;
        height: 100%;
        border: none;
        background-color: transparent;
        padding-left: 100px;
        box-sizing: border-box;
        font-size: 25px;
        color: #ffffff;

        &::placeholder{
            font-size: 25px;
            color: #ffffff;
        }
    }
`

const List = styled.ul`

`

const Item = styled.li`
    background-color: transparent;
    height: 70px;
    list-style: none;
    text-align: left;
    border-bottom: 1px solid #ffffff;
    transition: .2s linear;
    position: relative;

    a{
        font-size: 18px;
        color: #ffffff;
        text-decoration: none;
        width: 100%;
        height: 100%;
        padding-left: 100px;
        display: flex;
        align-items: center;
        box-sizing: border-box;
    }

    img{
        position: absolute;
        left: 40px;
        opacity: .3;
        transition: .2s linear;
    }

    &:hover{
        background-color: ${props => props.color};
 
        img{ 
            opacity: 1;
        }
    }

    &:first-child{
        border-top: 1px solid #ffffff;
    }

    @media(max-width: 764px){
        height: 59px;
    }
`

const HeaderComponent = ({ state }) => {

    const acf_data = state.source.get('/acf/')
    const acf = state.source[acf_data.type][acf_data.id]['acf']['header']

    const [isOpened, changeIsOpened] = useState(false)

    const [links, changeLinks] = useState(acf.links)
    
    const changeFilterName = (value) => {
        changeLinks(acf.links.filter(el => el.name.toLowerCase().includes(value.toLowerCase())))
    }

    return (
        <Header>
            <Container>
                <Flex>
                    <Logo>
                        <NavLink link="/">Logo</NavLink>
                    </Logo>
                    <Menu isOpened={isOpened} onClick={() => { changeIsOpened(!isOpened) }}>
                        <span />
                    </Menu>
                </Flex>
                <Navigation isOpened={isOpened}>
                    <Close onClick={() => { changeIsOpened(!isOpened) }} />
                    <Search>
                        <input placeholder='Szukaj' onChange={event => changeFilterName(event.target.value)}/>
                    </Search>
                    <List>
                        {links.map((el, index) =>
                            <Item key={index} color={el.color}>
                                <Link link={el.link}>
                                    {el.icon && <img src={el.icon} alt='kategoria' />}
                                    {el.name}
                                </Link>
                            </Item>
                        )}
                    </List>
                </Navigation>
            </Container>
        </Header>
    )
}

export default connect(HeaderComponent)

