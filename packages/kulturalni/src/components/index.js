import React from "react"
import { connect, Global, css } from "frontity"
import Switch from "@frontity/components/switch"
import Header from "./header/Header"
import Footer from "./footer/Footer"
import MainPage from "./main-page/MainPage"
import CategoryPage from "./category-page/Category"
import CategoryFilterPage from "./category-filter-page/Category"
import ItemPage from "./item-page/Item"
import Loader from "./loader/Loader"
import Error from "./error/Error"
import Reklama from "./reklama/Reklama"
import Kontakt from "./contact/Kontakt"

const Root = ({ state }) => {

    const data = state.source.get(state.router.link)
    let item
    if (data.type) {
        item = state.source[data.type][data.id]
    }

    // const acf_data = state.source.get('/acf/')
    return (
        <React.Fragment>
            
            <Global styles={css` 
            @import url('https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap&family=Poppins:wght@400;500;600;700&display=swap&family=Roboto&display=swap');
            * {
                padding: 0;
                margin: 0;
                scroll-behavior: smooth;
                font-family: 'Roboto', sans-serif;
            } 
            body{
                overflow-x: hidden;
            }
            main{
                margin-top: 84px;
            }
            `} />
            
            <Header />
            <Switch>
                <MainPage when={state.router.link == '/'} />
                <CategoryFilterPage when={data.isCategoryExtended} />
                <CategoryPage when={data.isCategory} />
                <ItemPage when={data.isPost} />
                <Error when={data.is404} />
                <Reklama when={state.router.link.includes('/reklama/')} />
                <Kontakt when={state.router.link.includes('/kontact/')} />
                <Loader select={true} />
            </Switch>
            <Footer />
        </React.Fragment>
    )
}

export default connect(Root)