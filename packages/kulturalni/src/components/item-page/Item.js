import React from 'react'
import { connect, styled } from "frontity"
import Content from './components/Content'
import Adds from './components/Adds'
import { Container } from '../../common/styles'

const Wrapper = styled.section`
    background-image: url(${props => props.BackGround});
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    min-height: 600px;
    position: relative;
    display: flex;
    justify-content: center;
    align-items: flex-end;
`

const Title = styled.h1`
    margin-bottom: 100px;
    color: #ffffff;
    font-size: 30px;
    text-align: center;
    max-width: 1080px;
    margin: 0 auto 100px;
`

const Flex = styled.div`
    display: grid;
    grid-template-columns: 3fr 1fr;
    grid-column-gap: 90px;

    @media(max-width: 1540px){
        grid-template-columns: 1fr;
        max-width: 1040px;
        margin: 0 auto;
    }
`

const ItemPage = ({ state, libraries }) => {
    const Html2React = libraries.html2react.Component;


    let data = state.source.get(state.router.link)
    let item = state.source[data.type][data.id]
    

    return (
        <main>
            <Wrapper BackGround={item.acf.img}>
                <Title>{item.acf.cytat}</Title>
            </Wrapper>
            <Container>
                <Flex>
                    <Content state={state} item={item} Html2React={Html2React} />
                    <Adds state={state} item={item} />
                </Flex>
            </Container>
        </main>
    )
}

export default connect(ItemPage)