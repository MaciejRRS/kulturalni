import React from 'react'
import { styled } from "frontity"
import { Container } from '../../../common/styles'

const Text = styled.p`
    text-transform: uppercase;
    font-size:10px;
    color: #aaa;
`

const Link = styled.a`
    display: flex;
    justify-content: center;
    align-items: center;

    img{
        max-width: 100%;
        max-height: 200px;
    }
`


const Add = (props) => {
    let el = props.elements[props.id]
    return (
        <>
            <Text>Reklama</Text>
            <Link href={el.link}>
                <img src={el.img} alt={'reklama'} />
            </Link>
        </>
    )
}

export default Add