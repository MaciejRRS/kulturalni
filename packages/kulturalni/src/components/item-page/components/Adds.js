import React, { useState, useEffect } from 'react'
import axios from 'axios'
import LayoutItem from './LayoutItem'
import { styled } from "frontity"
import { CheckForPass, ChooseAdds } from './../../../common/functions/Adds'
import Add from './AddItem'

const Wrapper = styled.aside`
    margin: 30px 0;
    border: 1px solid #EAEAEA;
    box-sizing: border-box;
    height: fit-content;
    padding: 0 30px 30px 30px;

    @media(max-width: 1198px){
        border: none;
        padding: 0;
    }
`

const Grid = styled.div`
    display: grid;
    grid-row-gap: 30px;
    margin-bottom: 30px;

    @media(max-width: 1540px){
        grid-template-columns: 1fr 1fr 1fr;
        grid-column-gap: 16px;
    }

    @media(max-width: 764px){
        grid-template-columns: 1fr;
        grid-row-gap: 16px;
    }
`

const GridTitle = styled.h2`
    margin: 20px 0 30px 0;
    font-weight: normal;
    font-size: 30px;
    color: #707070;
    font-family: 'Poppins', sans-serif;
    font-weight: bold;
`

const Adds = (props) => {
    // nowosci 5 i polecane 25
    let categories = props.state.source.get('categories')
    let category_extended = props.state.source.get('category_extended')

    const [postsNowosci, changePostsNowosci] = useState([])
    const [postsPolecane, changePostsPolecane] = useState([])
    let newsCategory = categories.items.filter(el => el.id == 5)
    let polecaneCategory = categories.items.filter(el => el.id == 25)

    useEffect(() => {

        let one = `https://kulturalni-frontity.kulturalni.civ.pl/wp-json/wp/v2/posts?categories=5&per_page=4`
        let two = `https://kulturalni-frontity.kulturalni.civ.pl/wp-json/wp/v2/posts?categories=25&per_page=4`

        const requestOne = axios.get(one)
        const requestTwo = axios.get(two)

        axios.all([requestOne, requestTwo]).then(axios.spread((...responses) => {
            changePostsNowosci(responses[0].data.filter(el => el.id != props.item.id))
            changePostsPolecane(responses[1].data.filter(el => el.id != props.item.id))
        }))

    }, [])

    // adds function

    let adds = props.state.source.get('adds')
    adds = adds.acf.sidebar.adds
    let numberOfAdds = 2

    let passedAdds = CheckForPass(adds)
    let chosenAdds = ChooseAdds(passedAdds, numberOfAdds)

    // adds function

    return (
        <Wrapper>
            <GridTitle>Nowości</GridTitle>
            <Grid>
                {postsNowosci.map(el =>
                    <LayoutItem el={el} state={props.state} currentCategory={newsCategory} />
                )}
            </Grid>

            <facebook></facebook>

            <Add elements={chosenAdds} id={'0'} />

            <GridTitle>Polecamy</GridTitle>
            <Grid>
                {postsPolecane.map(el =>
                    <LayoutItem el={el} state={props.state} currentCategory={polecaneCategory} />
                )}
            </Grid>

            <Add elements={chosenAdds} id={'1'} />

        </Wrapper>
    )
}

export default Adds