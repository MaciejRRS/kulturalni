import React, { useState, useEffect } from 'react'
import { styled } from "frontity"
import Date from './../../../common/sprites/date.png'
import Time from './../../../common/sprites/time.png'
import Place from './../../../common/sprites/place.png'
import { CheckForPass, ChooseAdds } from './../../../common/functions/Adds'
import Add from './AddItem'
import Link from "@frontity/components/link"
import Terms from './Terms'
import axios from 'axios'
import LayoutItem from './LayoutItem'

const Flex = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin: 60px 0;

    @media(max-width: 1198px){
        flex-direction: column;
        align-items: flex-start;
        margin: 60px 0 0;
    }
    @media(max-width: 764px){
        margin: 0;
    }
`

const ParamFLex = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    p{
        margin-left: 30px;
    }

    @media(max-width: 764px){
        margin: 15px 0;
    }
`

const CategoryLink = styled.p`
    text-transform: capitalize;
        margin-top: 60px;
    a{
        color: ${props => props.color};
        font-family: 'Poppins';
        font-size: 15px;
        cursor: pointer;
        text-decoration: none;
    }
`

const Title = styled.h1`
    font-family: 'Poppins';
    font-size: 30px;

`

const Grid = styled.div`
    display: grid;
    grid-template-columns: 1fr 1fr;
    grid-column-gap: 50px;
    grid-row-gap: 50px;
    margin: 60px 0 90px;

    @media(max-width: 1198px){
        grid-column-gap: 16px;
        grid-row-gap: 16px;
    }

    @media(max-width: 764px){
        grid-template-columns: 1fr;
    }
`

const MainImg = styled.img`
    float: right;
    margin-left: 50px;
    margin-bottom: 20px;
    max-height: 450px;

    @media(max-width: 1198px){
        display: none;
    }
`

const ContentWrapper = styled.div`
    p{
        margin-top: 50px;
        font-family: 'Roboto', sans-serif;
        font-size: 15px;
        line-height: 20px;
        color: #707070;
    }
`

const TabletFlex = styled.div`
    a, .img{
        display: none;
    }

    @media(max-width: 1198px){
        display: grid;
        grid-template-columns: 1fr 1fr;
        grid-column-gap: 16px;
        margin-top: 30px;
        .img{
            display: block;
            margin: 0 auto;
            max-width: 100%;
            max-height: 400px;
        }

        a{
            display: flex;
            justify-content: center;
            align-items: center;
            text-decoration: none;
            color: #fff;
            line-height: 27px;
            background-color: ${props => props.color};
            width: 100%;
            height: 37px;
        }
    }

    @media(max-width: 764px){
        margin-top: 0px;
        grid-row-gap: 30px;
        grid-template-columns: 1fr;

        a{
            margin-top: 15px;
        }
    }
`

const Content = (props) => {

    let categories = props.state.source.get('categories')
    let category_extended = props.state.source.get('category_extended')

    let ExtendedCategory = false

    const choseCategory = () => {
        let chosen
        if (props.item.category_extended.length) {
            for (let i = 0; i <= category_extended.items.length; i++) {
                if (category_extended.items[i].id == props.item.category_extended[0]) {
                    chosen = category_extended.items[i]
                    ExtendedCategory = true
                    return chosen
                }
            }
        } else {
            for (let i = 0; i <= categories.items.length; i++) {
                if (categories.items[i].id == props.item.categories[0]) {
                    chosen = categories.items[i]
                    ExtendedCategory = false
                    return chosen
                }
            }
        }
    }

    let [currentCategory, changecurrentCategory] = useState([choseCategory()])
    let [postsOfCategory, changePostsOfCategory] = useState([])

    useEffect(() => {
        let one = ExtendedCategory
            ? `https://kulturalni-frontity.kulturalni.civ.pl/wp-json/wp/v2/posts?category_extended=${currentCategory[0].id}&per_page=5`
            : `https://kulturalni-frontity.kulturalni.civ.pl/wp-json/wp/v2/posts?categories=${currentCategory[0].id}&per_page=5`

        const requestOne = axios.get(one)

        axios.all([requestOne]).then(axios.spread((...responses) => {
            changePostsOfCategory(responses[0].data.filter(el => el.id != props.item.id))
        }))

    }, [])



    // adds function

    let adds = props.state.source.get('adds')
    adds = adds.acf.content.adds
    let numberOfAdds = 1

    let passedAdds = CheckForPass(adds)
    let chosenAdds = ChooseAdds(passedAdds, numberOfAdds)

    // adds function
    return (
        <section>
            <CategoryLink color={currentCategory[0].acf.color}>
                <Link>{currentCategory[0].name}</Link>
            </CategoryLink>
            <Title>{props.item.title.rendered}</Title>
            <TabletFlex color={currentCategory[0].acf.color}>
                <Flex>
                    <ParamFLex >
                        <img src={Date} alt="data" />
                        <p>
                            {props.item.acf.date_time[0].date}
                            {' - '}
                            {props.item.acf.date_time[props.item.acf.date_time.length - 1].date}
                        </p>
                    </ParamFLex>
                    <ParamFLex >
                        <img src={Time} alt="czas" />
                        <p>
                            {props.item.acf.date_time[0].time}
                        </p>
                    </ParamFLex>
                    <ParamFLex >
                        <img src={Place} alt="miejsce" />
                        <p>{props.item.acf.place}</p>
                    </ParamFLex>
                    <Link link="">sprawdź wszystkie terminy</Link>
                </Flex>
                <img className="img" src={props.item.acf.img_main} />
            </TabletFlex>
            <div>
                <MainImg src={props.item.acf.img_main} />
                <ContentWrapper>
                    <props.Html2React html={props.item.content.rendered} />
                </ContentWrapper>
                <Terms item={props.item} />
                <Add elements={chosenAdds} id={'0'} />
                <ContentWrapper>
                    <props.Html2React html={props.item.acf.content} />
                </ContentWrapper>
                <Grid>
                    {postsOfCategory.map(el =>
                        <LayoutItem el={el} state={props.state} currentCategory={currentCategory} />
                    )}
                </Grid>
            </div>
        </section>
    )
}

export default Content