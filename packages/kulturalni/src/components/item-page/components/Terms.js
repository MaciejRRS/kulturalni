import React from 'react'
import { styled } from "frontity"

const Text = styled.p`
    text-transform: uppercase;
    font-size:10px;
    color: #aaa;
`

const TitleGrid = styled.div`
    display: grid;
    grid-template-columns: 1.5fr 3fr 2fr 1fr 1fr;
    position: relative;
    border-bottom: 1px solid #EE732D;
    padding: 10px;
    margin: 15px 0;
    font-size: 20px;
    font-weight: bold;
    color: #707070;

    @media(max-width: 1050px){
        padding: 0px;
        margin: 0 0 15px 0;

        div{
            display: none;
        }
    }
`

const Grid = styled.div`
    display: grid;
    grid-template-columns: 1.5fr 3fr 2fr 1fr 1fr;
    padding: 10px;
    margin: 7px 0 8px;
    transition: .1s linear;
    div{
        display: flex;
        align-items: center;
        padding: 0 3px;
    }
    &:hover{
        background-color: #EDEDED;
    }

    .mobile_time{
        display: none;
    }

    @media(max-width: 1050px){
        display: flex;
        align-items: center;
        flex-direction: column;
        position: relative;
        padding: 10px 10px 30px 10px;

        .mobile_time{
            display: block;
        }

        .desctop_time{
            display: none;
        }

        .place{
            margin: 10px 0 15px;
            color: #969696;
        }

        .title{
            margin-top: 10px;
            color: #707070;
        }

        .date{
            color: #EE732D;
            font-size: 12px;
        }

        &::after{
            position: absolute;
            bottom: 0;
            left: 0;
            content: "";
            width: 100%;
            height: 1px;
            background-color: #EE732D;
        }
    }
`

const Button = styled.a`
    text-align: center;
    background-color: #2D2D2D;
    color: #fff;
    padding: 0 50px;
    border: 1px solid #2D2D2D;
    line-height: 25px;
    font-size: 15px;
    display: block;
    cursor: pointer;
    transition: .15s linear;
    text-decoration: none;
    text-transform: uppercase;
    &:hover{
        color: #2D2D2D;
        background-color: #fff;
    }
`

const Title = styled.h2`
    font-size: 20px;
    font-weight: bold;
    color: #2D2D2D;
    margin-top: 36px;
    font-family: 'Poppins';
    text-transform: uppercase;
`

const Wrapper = styled.div`
    margin-bottom: 60px;
    clear: both;
`

const Content = styled.div`
    @media(max-width: 1050px){
        grid-template-columns: 1fr 1fr;
        grid-column-gap: 16px;
        display: grid;
    }

    @media(max-width: 600px){
        grid-template-columns: 1fr ;
    }
`

const Terms = (props) => {
    return (
        <Wrapper>
            <Title>Sprawdź wszystkie dostępne terminy</Title>
            <TitleGrid>
                <div>Data</div>
                <div>Wydarzenie</div>
                <div>Miejsce</div>
                <div>Godz.</div>
                <div></div>
            </TitleGrid>
            <Content>
                {props.item.acf.date_time.map(el =>
                    <Grid>
                        <div className="date">{el.date}<span className="mobile_time">{', ' + el.time}</span></div>
                        <div className="title">{props.item.title.rendered}</div>
                        <div className="place">{props.item.acf.place}</div>
                        <div className="desctop_time">{el.time}</div>
                        <div><Button href={el.link}>bilety</Button></div>
                    </Grid>
                )}
            </Content>
        </Wrapper>
    )
}

export default Terms