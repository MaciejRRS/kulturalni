import React from 'react';
import { styled } from "frontity"

const Spin = styled.section`
    height: calc(100vh - 376px);
`

const Loader = () => {
    return(
        <Spin></Spin>
    )
}

export default Loader