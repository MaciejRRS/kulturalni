import React, { useState } from 'react'
import { connect } from "frontity"
import Layout from './components/Layout'
import Add from './components/Add'
import {CheckForPass, ChooseAdds} from './../../common/functions/Adds'
import { styled } from "frontity"

const Main = styled.div`
    margin-bottom: 60px;
`

const MainPage = ({ state }) => {

    const acf_data = state.source.get('/acf/')
    const acf = state.source[acf_data.type][acf_data.id]['acf']['main_page']

    let categories = state.source.get('categories')
    let category_extended = state.source.get('category_extended')

    // adds function

    let adds = state.source.get('adds')
    adds = adds.acf.home.adds
    let numberOfAdds = acf.sections.length

    let passedAdds = CheckForPass(adds)
    let choosenAdds = ChooseAdds(passedAdds, numberOfAdds)

    // adds function 

    return (
        <Main>
            {acf.sections.map((el, index) =>
                <>
                    <Layout key={index} state={state} categories={categories} tabs={el.tabs} />
                    <Add elements={choosenAdds} id={index} />
                </>
            )}
        </Main>
    )
}

export default connect(MainPage)