import React, { useState, useEffect } from 'react'
import { styled } from "frontity"
import { Container } from '../../../common/styles'
import LayoutItem from './LayoutItem'
import axios from 'axios'

const Section = styled.section`
    margin-top: 120px;
    @media(max-width: 1196px){
    margin-top: 60px;
    }
    @media(max-width: 764px){
        margin-top: 30px;
    }
`

const Grid = styled.div`
    display: grid;
    grid-template-columns: 2fr 1fr 1fr;
    grid-column-gap: 50px;
    grid-row-gap: 50px;
    @media(max-width: 1600px){   
        grid-column-gap: 16px;
        grid-row-gap: 16px;
    }
    @media(max-width: 1196px){
        grid-template-columns: 1fr 1fr;
    }
    @media(max-width: 600px){
        grid-template-columns: 1fr;
    }
`

const Tabs = styled.div`
    min-height: 80px;
    width: 100%;
    display: grid;
    grid-template-columns: 1fr 1fr 1fr 1fr 1fr 1fr;
    grid-column-gap: 3%;
    position: relative;
    transition: .2s linear;
    margin-bottom: 50px;

    @media(max-width: 1600px){
        margin-bottom: 30px;
    }

    @media(max-width: 1196px){
        grid-template-columns: 1fr 1fr 1fr;
        min-height: 160px;
    }

    @media(max-width: 764px){
        grid-template-columns: 1fr 1fr;
        grid-row-gap: 9px;
        grid-column-gap: 9px;
        height: auto;
    }

    &::after{
        content: '';
        height: 10px;
        position: absolute;
        bottom: 0;
        left: 0;
        right: 0;
        background-color: ${props => props.color};
        transition: .2s linear;

        @media(max-width: 1196px){
            bottom: 80px;
        }
        @media(max-width: 764px){
            display: none;
        }
    }
`

const Tab = styled.div`
    height: 70px;
    display: flex;
    align-items: center;
    justify-content: center;
    background-color: ${props => props.color ? props.color : '#EAEAEA'};
    transition: .2s linear;
    cursor: pointer;

    span{
        margin-left: 10px;
    }

    &:hover{
        box-shadow: 0 3px 12px 0 #00000066;
    }

    &.active{
        color: #ffffff;
        cursor: unset;
    }

    @media(max-width: 764px){
        font-size: 12px;
        height: 45px;
    }
`

const AbsoluteText = styled.h2`
    position: absolute;
    
    transform-origin: right;
    transform: rotateZ(-90deg) translateY(-50%);
    right: 0;
    font-size: 90px;
    color: ${props => props.color};
    opacity: .2;
    font-weight: 300;
    @media(max-width: 764px){
        position: unset;
        transform: unset;
        text-align: center;
        text-transform: uppercase;
        font-size: 12vw;
        margin-bottom: 10px;
    }
`

const Description = styled.p`
    margin: 30px 0;
`


const Hero = (props) => {

    const [currentTab, changeCurrentTab] = useState(props.tabs[0])
    let [posts, changePosts] = useState([])
    let [category, changeCategory] = useState('')

    const [isFetching, changeIsFetching] = useState(false)

    useEffect(() => {
        let currentTabSlag = currentTab.tab_link.slice(10, -1)
        let currentCategory

        props.categories.items.forEach(el => {
            if (el.slug === currentTabSlag) {
                currentCategory = el
            }
        });

        // props.category_extended.items.forEach(el => {
        //     if (el.slug === currentTabSlag) {
        //         currentCategory = el
        //     }
        // });

        if (currentCategory) {
            let one = `https://kulturalni-frontity.kulturalni.civ.pl/wp-json/wp/v2/posts?categories=${currentCategory.id}&per_page=8`
            let two = `https://kulturalni-frontity.kulturalni.civ.pl/wp-json/wp/v2/categories/${currentCategory.id}`

            const requestOne = axios.get(one)
            const requestTwo = axios.get(two)

            axios.all([requestOne, requestTwo]).then(axios.spread((...responses) => {
                changePosts(responses[0].data)
                changeCategory(responses[1].data)
                changeIsFetching(false)
            }))
        } else {
            console.log('chosen category doesnt exist')
            changeIsFetching(false)
        }
    }, [currentTab])

    const changeTab = (tab) => {
        changeCurrentTab(tab)
        changeIsFetching(true)
    }
    return (
        <Section>
            <Container>
                <AbsoluteText color={currentTab.color}>
                    {currentTab.text}
                </AbsoluteText>
                <Tabs color={currentTab.color}>
                    {props.tabs.map(el =>
                        <>
                            {
                                currentTab.color == el.color
                                    ? <Tab className="active" color={el.color}>
                                        <img src={el.tab_icon_active} alt='category' />
                                        <span>{el.tab_name}</span>
                                    </Tab>
                                    : <Tab onClick={() => { changeTab(el) }}>
                                        <img src={el.tab_icon} alt='category' />
                                        <span>{el.tab_name}</span>
                                    </Tab>
                            }
                        </>
                    )}
                </Tabs>
                <Grid>
                    {posts.map((el, index) =>
                        <>
                            {index < 8
                                ? <LayoutItem isFetching={isFetching} el={el} state={props.state} currentCategory={category} />
                                : null
                            }
                        </>
                    )}
                </Grid>
                <Description>
                    {category.description}
                </Description>
            </Container>
        </Section>
    )
}

export default Hero