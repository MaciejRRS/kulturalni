import React from 'react'
import { styled } from "frontity"
import Link from "@frontity/components/link"

const Item = styled.div`
    position: relative;
    width: 100%;
    min-height: 248px;
    background: url(${props => props.isFetching ? 'unset' : props.background});
    background-position: center;
    background-size: cover;
    background-repeat: no-repeat;

    &:nth-child(1){
        grid-row-start: 1;
        grid-row-end: 3;
        min-height: 546px;

        h3{
            font-size: 20px;
        }

        @media(max-width: 1600px){   
            min-height: 512px;
        }

        @media(max-width: 1196px){
            grid-row-start: unset;
            grid-row-end: unset;
            min-height: 248px;
        }
    }

    &::after{
        display: ${props => props.isFetching ? 'block' : 'none'};
        content: "";
        position: absolute;
        left: 0;
        bottom: 0;
        right: 0;
        top: 0;
        background-color: rgba(0, 0, 0, 0.13);
    }
`


const ItemTitle = styled.h3`
    position: absolute;
    bottom: 15px;
    left: 15px;
    right: 15px;
    font-size: 15px;
    font-weight: bold;
    color: #ffffff;
`

const Icon = styled.div`
    position: absolute;
    left: 0;
    top: 0;
    background-color: ${props => props.color};
    width: 92px;
    height: 92px;
    display: ${props => props.isFetching ? 'none' : 'flex'};;
    justify-content: center;
    align-items: center;
    border-bottom-right-radius: 15px;
    
    img{
        max-width: 50%;
        max-height: 50%;
        width: 100%;
    }
    @media(max-width: 1198px){
        width: 62px;
        height: 62px;
    }
`

const NavLink = styled(Link)`
    position: absolute;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
`

const LayoutItem = (props) => {
    return (
        <Item background={props.el.acf.img} isFetching={props.isFetching}>
            <NavLink link={props.el.link} >
                <Icon isFetching={props.isFetching} color={props.currentCategory.acf.color}>
                    <img src={props.currentCategory.acf.icon} alt="ikona kategorii" />
                </Icon>
                <ItemTitle>{props.el.title.rendered}</ItemTitle>
            </NavLink>
        </Item>
    )
}

export default LayoutItem