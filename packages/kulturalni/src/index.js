import Root from './components/index'

// https://kulturalni-frontity.kulturalni.civ.pl/wp-json/wp/v2/categories

const categoriesHandler = {
  pattern: "categories",
  func: async ({ route, state, libraries }) => {
    const response = await libraries.source.api.get({
      endpoint: `/wp/v2/categories?per_page=99`
    });
    const items = await response.json();

    const data = state.source.get(route);
    Object.assign(data, { items, isCategories: true });
  }
}

const categoriesExtendedHandler = {
  pattern: "category_extended",
  func: async ({ route, state, libraries }) => {
    const response = await libraries.source.api.get({
      endpoint: `/wp/v2/category_extended?per_page=99`
    });
    const items = await response.json();

    const data = state.source.get(route);
    Object.assign(data, { items, isCategories: true });
  }
}

const addsHandler = {
  pattern: "adds",
  func: async ({ route, state, libraries }) => {
    const response = await libraries.source.api.get({
      endpoint: `/acf/v3/options/options`
    });
    const option = await response.json();

    const data = state.source.get(route);
    Object.assign(data, { ...option, isAcfOptionsPage: true });
  }
}

export default {
  name: "kulturalni",
  roots: {
    theme: Root
  },
  state: {
    theme: {}
  },
  actions: {
    theme: {
      beforeSSR: async ({ state, actions }) => {
        await Promise.all([
          actions.source.fetch("/acf/"),
          actions.source.fetch("categories"),
          actions.source.fetch('category_extended'),
          actions.source.fetch('adds')
        ]);
      }
    }
  },
  libraries: {
    source: {
      handlers: [categoriesHandler, categoriesExtendedHandler, addsHandler]
    }
  }
};
